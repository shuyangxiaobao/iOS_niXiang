## 第6天 Mach-O 文件
[百度](http://www.baidu.com)


在 .zshrc 里面添加这句  导入了  bash_profile里面的配置
#
>      source ~/.bash_profile 


切换shell 脚本为 bash
#
>     chsh -s /bin/bash

把 Zsh 设置为当前用户的默认 Shell
#
>     chsh -s /bin/zsh


导出微信头文件
利用class-dump 导出 app包的头文件. wechatHeader 是文件夹名字  根 WeChat 在同级目录。
#
     class-dump -H  WeChat -o wechatHeader
#
     查看文件类型  file test.o
#
     1.编译  test.c 生成  test.o 文件(.o 文件叫目标文件)
        clang -c test.c
#
     2.把test.o 目标文件转化成 a.out 可执行文件
    clang test.o
#
     3.执行 a.out 文件
    ./a.out
#
    4.把test.o 目标文件转化成 testhaha 可执行文件
    clang -o testhaha test.o
#
    5.把test.c 一步转化成 testhaha 可执行文件
    clang -o testhaha2 test.c

#
    6.把 test.c 和 test1.c 两个文件 转化为一个 nimeide 可执行文件。
      clang -o nimeide test.c test1.c
查找.a 静态库
两种方式
#
    1.
    cd /usr/lib 
    find . -name "*.a”
#   
    2.
    find /usr/lib  -name "*.a”

查看文件信息
#
    file /usr/lib/libkmod.a

清空终端命令
# 
>     clear
#
>     （.a  是由一堆的.o 组成）





