#007--LLDB(Low Lever Debug)


###断点
* 设置断点
$breakpoint set -n XXX
set 是子命令
-n 是选项 是--name 的缩写!

* 查看断点列表
$breakpoint list 

* 删除
$breakpoint delete 组号

* 禁用/启用
$breakpoint disable 禁用
$breakpoint enable  启用

* 遍历整个项目中满足Game:这个字符的所有方法
$breakpoint set -r Game:
###流程控制
* 继续执行
$continue c 
* 单步运行,将子函数当做整体一步执行
$n next
* 单步运行,遇到子函数会进去
$s 

###stop-hook
让你在每次stop的时候去执行一些命令,只对breadpoint(代码断点),watchpoint(内存断点)
pause  debug view  这两种触发LLDB 不会执行stop-hook

> ###### target stop-hook add -o "frame variable"  打印所有方法参数
> ###### target stop-hook list
> ######  target stop-hook delete
> target stop-hook add -o "self.view"
> undisplay 2    干掉某一组的hook
> 
> /Users/geqiangbao/.lldbinit    输入内容：  target stop-hook add -o "frame variable"
> 
> 
>  image lookup -a 0x183c53fd8
> image lookup -t Person  快速查看这个类
> image list
> 
> frame variable
> 
> memory read 0x000000010be0cf30   ==    x 0x000000010be0cf30


###常用命令
* image list 
* p  执行
* b -[xxx xxx]   断点
* x 
* register read     读寄存器
* po        打印


###关于后期的课程安排(非越狱!)

OC反汇编(MachO文件的东西)
密码学(Base64,HASH,RSA)
苹果签名技术
重签名 Xcode签名 脚本自动签名
动态库(动态库的注入)
HOOK原理 


